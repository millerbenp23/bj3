import React from 'react';

const CountingSystemSelector = ({ countingSystem, setCountingSystem }) => {
  const handleChange = (event) => {
    setCountingSystem(event.target.value);
  };

  return (
    <div>
      <label htmlFor="countingSystem">Select a counting system: </label>
      <select id="countingSystem" value={countingSystem} onChange={handleChange}>
        <option value="Hi-Lo">Hi-Lo</option>
        <option value="Hi-Opt I">Hi-Opt I</option>
        <option value="KISS III">KISS III</option>
        <option value="KO (Knock-Out)">KO (Knock-Out)</option>
        <option value="Omega II">Omega II</option>
        <option value="Red 7">Red 7</option>
        <option value="Silver Fox">Silver Fox</option>
        <option value="Zen Count">Zen Count</option>
      </select>
    </div>
  );
};

export default CountingSystemSelector;

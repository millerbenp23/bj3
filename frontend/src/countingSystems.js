const hiLo = (card) => {
  if (card >= 2 && card <= 6) return 1;
  if (card >= 7 && card <= 9) return 0;
  return -1;
};

const hiOpt1 = (card) => {
  if (card === 3 || card === 4 || card === 6) return 1;
  if (card === 2 || card === 7 || card === 8 || card === 9) return 0;
  return -1;
};

const kissIII = (card, remainingDecks, initialDecks) => {
  if (card >= 2 && card <= 6) return 1;
  if (card >= 7 && card <= 9) return 0;
  if (card === 11 && remainingDecks < initialDecks / 2) return -1;
  return -1;
};

const knockOut = (card) => {
  if (card >= 2 && card <= 7) return 1;
  if (card === 8 || card === 9) return 0;
  return -1;
};

const omegaII = (card) => {
  if (card === 2 || card === 3 || card === 7) return 1;
  if (card === 8 || card === 9) return 0;
  if (card === 4 || card === 5 || card === 6) return 2;
  if (card === 11) return -1;
  return -2;
};

const red7 = (card, isRed) => {
  if (card >= 2 && card <= 6) return 1;
  if (card === 8 || card === 9 || (card === 7 && !isRed)) return 0;
  return -1;
};

const silverFox = (card) => {
  if (card >= 2 && card <= 8) return 1;
  if (card === 9) return 0;
  return -1;
};

const zenCount = (card) => {
  if (card === 2 || card === 3 || card === 7) return 1;
  if (card === 4 || card === 5 || card === 6) return 2;
  if (card === 8 || card === 9) return 0;
  if (card === 11) return -1;
  return -2;
};

const getCardValue = (card) => {
  if (['J', 'Q', 'K'].includes(card)) return 10;
  if (card === 'A') return 11;
  return parseInt(card, 10);
};

const getCardColor = (card) => {
  return ['2♥', '3♥', '4♥', '5♥', '6♥', '7♥', '8♥', '9♥', '10♥', 'J♥', 'Q♥', 'K♥', 'A♥', '2♦', '3♦', '4♦', '5♦', '6♦', '7♦', '8♦', '9♦', '10♦', 'J♦', 'Q♦', 'K♦', 'A♦'].includes(card);
};

const getCountingSystemFunction = (countingSystem) => {
  switch (countingSystem) {
    case 'Hi-Opt I':
      return hiOpt1;
    case 'KISS III':
      return kissIII;
    case 'KO (Knock-Out)':
      return knockOut;
    case 'Omega II':
      return omegaII;
    case 'Red 7':
      return red7;
    case 'Silver Fox':
      return silverFox;
    case 'Zen Count':
      return zenCount;
    default:
      return hiLo;
  }
};

export { getCardValue, getCardColor, getCountingSystemFunction };
